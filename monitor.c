#include "monitor.h"
extern char *optarg;
user_t *array_of_users = NULL;

void usage(void){

	printf(
	    "\n"
	    "Options:\n"
	    "    -m, Print malicious users\n"
	    "    -i <filename>, Print table of users that modified the file <filename> and the number of modifications\n"
        "    -h  This help message\n" 
	);
	exit(EXIT_FAILURE);
}

/* returns the field num */
const char* getfield(char* line, int num) {

    const char* tok;
    for (tok = strtok(line, ",");
            tok && *tok;
            tok = strtok(NULL, ",\n"))
    {
        if (!--num)
            return tok;
    }
    return NULL;
}

/* check if this user is already inserted at list */
int has_user(int uid){

    user_t *tmp;
    if(array_of_users == NULL) return 0;

    tmp = array_of_users;
    while(tmp != NULL){
        if (tmp->uid == uid) {
            return 1;
        }
        tmp = tmp->next;   
    }
    return 0;
}

void insert_user(int uid){

    user_t *tmp;

    if(array_of_users == NULL){
        /* 1st node */
        array_of_users = malloc(sizeof(user_t));
        array_of_users->uid = uid;
        array_of_users->numfiles = 0;
        array_of_users->next = NULL;
        return ;
    }
    tmp = malloc(sizeof(user_t));
    tmp->uid = uid;
    tmp->numfiles = 0;
    tmp->next = array_of_users;
    array_of_users = tmp;
}

int insert_filename(const char *path){

    file_opened *tmp;

    if(!path) return 0;

    if(array_of_users->fopened == NULL){
        /* insert 1st node */
        array_of_users->fopened = malloc(sizeof(file_opened));
        array_of_users->fopened->name = strdup(path);
        array_of_users->fopened->next = NULL;

        //printf("insert name: %s\n", array_of_users->fopened->name);
    } else {
        tmp = array_of_users->fopened;
        while(tmp!=NULL){
            if(!strcmp(tmp->name, path)) return 0;
            tmp = tmp->next;
        }
        /* insert */
        tmp = malloc(sizeof(file_opened));
        tmp->name = strdup(path);
        tmp->next = array_of_users->fopened;
        array_of_users->fopened = tmp;

        //printf("insert name: %s\n", array_of_users->fopened->name);
    }
    return 1;
}

void find_malicious() {

    FILE* stream = fopen("logfile.csv", "r");

    char line[1024];
    int uid;
    user_t *tmparray;

    while (fgets(line, 1024, stream)){
        char* tmp = strdup(line);

        uid = atoi( getfield(tmp, 1) );

        if( !has_user(uid) ){ /* new uid */
            insert_user(uid);
        }
        free(tmp);
    }
    fclose(stream);
    stream = fopen("logfile.csv", "r");
    
    tmparray = array_of_users;
    /* to tmparray diatrexei olo ton pinaka */
    while(tmparray){
        uid = tmparray->uid;
        /* parse logfile for every user */
        while (fgets(line, 1024, stream)){

            char* tmp = strdup(line);

            if( atoi(getfield(tmp, 6)) == 1 ){ /* action_denied */
                tmp = strdup(line);
                if( insert_filename(getfield(tmp, 2)) ){ // den mporei na ksanakalestei gt xalaei to tmp
                    /* an to uid einai to trexon */
                    if(atoi(getfield(strdup(line), 1)) == uid) {
                        tmparray->numfiles++;
                    }
                }
                //printf("uid %d -> %d files\n",tmparray->uid, tmparray->numfiles);
            }
        }
        if(tmparray->numfiles >= 10){
            printf("UID %d tried to access %d different file(s) without permission\n", tmparray->uid, tmparray->numfiles);
        }
        tmparray = tmparray->next;
        //stream = fopen("logfile.csv", "r");
    }
    return;
}

void report_users(char *filename){
    printf("fname: %s\n", filename);
}

int main(int argc, char *argv[]){
    
    int opt;
    char *filename;

    /* get options */
	while ((opt = getopt(argc, argv, "mi:veh")) != -1) {
		switch (opt) {
            case 'm':
                find_malicious();
                break;
            case 'i':
                filename = strdup(optarg);
                report_users(filename);
                break;
            case 'v':

                break;
            case 'e':

                break;
            case 'h':
            default:
                usage();
		}
	}
    return 0;
}