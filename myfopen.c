#define _GNU_SOURCE
#include <limits.h> /* PATH_MAX */

#include "logfile.h"

/*
* path: string containing the name of the file to be opened
* mode: a file access mode (includes −)
*/
FILE *fopen(const char *path, const char *mode) {

    time_t t = time(NULL);
    struct tm time = *localtime(&t);
    int n, exists = 1;
    long length = 0;
    char buf[PATH_MAX + 1];
    char *rp /* real path */;
    char *filecontent = NULL;
    FILE *res;
    unsigned char digest[16];
    char *md5string = (char*)malloc(33);
    MD5_CTX context;

    if( access( path, F_OK ) == -1 ) exists = 0;

    FILE *(*original_fopen)(const char*, const char*);
    original_fopen = dlsym(RTLD_NEXT, "fopen");

    /* calling original fopen */
    res = (*original_fopen)(path, mode);

    rp = realpath(path, buf);
    if (!rp) {
        perror("\nrealpath");
        //exit(EXIT_FAILURE);
    }

    /* a file is created when it opens for writing */
    if(!strcmp(mode, "w")){
        
        /* and the filename does not already exist in the path 
           where it will be saved */
        if( !exists ) {
            printf("In our own fopen, creating %s\n", path);
            MD5_Init(&context);
            MD5_Update(&context, filecontent, 0);
            MD5_Final(digest, &context);

            for (n = 0; n < 16; ++n) {
                snprintf(&(md5string[n*2]), 16*2, "%02x", (unsigned int)digest[n]);
            }
            //printf("md5string: %s\n", md5string);
            
            logfile_update(buf, time, 0, 0, md5string);

        } else {
            printf("In our own fopen, opening %s\n", path);

            MD5_Init(&context);
            if(filecontent){
                MD5_Update(&context, filecontent, strlen(filecontent));
            }else{
                MD5_Update(&context, filecontent, 0);
            }
            MD5_Final(digest, &context);

            for (n = 0; n < 16; ++n) {
                snprintf(&(md5string[n*2]), 16*2, "%02x", (unsigned int)digest[n]);
            }
            //printf("md5string: %s\n", md5string);

            logfile_update(buf, time, 1, 0, md5string);
        }
    } /* else it opens for reading */
    else if(!strcmp(mode, "r")){
        if( !exists ) {
            /* the file must exist */
            printf("%s does not exist\n", path);
            logfile_update(buf, time, 1, 1, NULL);
        } else {
            /* read file content */
            if(res){
                fseek (res, 0, SEEK_END);
                length = ftell (res);
                fseek (res, 0, SEEK_SET);
                filecontent = malloc(length*sizeof(char));
                if (filecontent){
                    fread (filecontent, 1, length, res);
                    filecontent[length]='\0';
                }
            }
            /* workig on fingerprint.. */
            MD5_Init(&context);
            MD5_Update(&context, filecontent, strlen(filecontent));
            MD5_Final(digest, &context);

            /* hex representation as a string */
            for (n = 0; n < 16; ++n) {
                snprintf(&(md5string[n*2]), 16*2, "%02x", (unsigned int)digest[n]);
            }

            //printf("md5string: %s\n", md5string);

            logfile_update(buf, time, 1, 0, md5string);
        }
    }
    
    return res;
}