#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

typedef struct file_opened {
    char *name;
    struct file_opened *next;
} file_opened;

typedef struct user_t {
    int uid;
    int numfiles;
    file_opened *fopened;
    struct user_t *next;
} user_t;

typedef struct file_t {
    int uid;
    int modifications;
} file_t;
