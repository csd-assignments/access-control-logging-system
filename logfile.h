#include <stdio.h>
#include <dlfcn.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <openssl/md5.h>

typedef struct logentry_t {
    int uid;
    char *filename;
    char *date;
    char *time;
    int type;
    int action_denied;
    char *fingerprint;
} logentry_t;

int logfile_create(void);

/* update logfile with information regarding
   the file access/ modification attempt */
void logfile_update(const char *path, struct tm time, int type, int act, char *md5string);

/* make a new entry to logfile with information 
   about the access */
void newfile_entry(logentry_t *entry);