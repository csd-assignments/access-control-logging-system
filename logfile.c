#include "logfile.h"

int logfile_create(void){

    printf("Creating logfile.csv file\n");
 
    FILE *fp;
    
    fp = fopen("logfile.csv","w+");
    if(fp == NULL){
        printf("logfile creation failed\n");
        return 0;
    }
    
    fprintf(fp,"UID, file name, date, time, type, action_denied, fingerprint\n");

    fclose(fp);
    return 1;
}

/* Epistrefei to string "year-month-day" */
char* datestr(int year, int month, int day) {

	char *y, *m, *d, *final = NULL;

	y = (char*)malloc(sizeof(unsigned int));
	m = (char*)malloc(sizeof(int));
    d = (char*)malloc(sizeof(int));

	/* integer to string */
	sprintf(y, "%u", year);
	sprintf(m, "%02u", month);
    sprintf(d, "%02u", day);

	final = malloc(strlen(y) + strlen(m) + strlen(d) + 3);
    memset(final, 0, strlen(y) + strlen(m) + strlen(d));

	strcat(final, y);
	strcat(final, "-");
	strcat(final, m);
    strcat(final, "-");
    strcat(final, d);

	return final;
}

/* Epistrefei to string "hour:min:sec" */
char* timestr(int hour, int min, int sec) {

	char *h, *m, *s, *final = NULL;

	h = (char*)malloc(sizeof(int));
	m = (char*)malloc(sizeof(int));
    s = (char*)malloc(sizeof(int));

	/* integer to string */
	sprintf(h, "%02u", hour);
	sprintf(m, "%02u", min);
    sprintf(s, "%02u", sec);

	final = malloc(strlen(h) + strlen(m) + strlen(s) + 3);
    memset(final, 0, strlen(h) + strlen(m) + strlen(s));

	strcat(final, h);
	strcat(final, ":");
	strcat(final, m);
    strcat(final, ":");
    strcat(final, s);

	return final;
}

/* update logfile with information regarding
   the file access/ modification attempt */
void logfile_update(const char *path, struct tm time, int type, int act, char *md5string){

    logentry_t *entry;

    entry = (logentry_t*)malloc(sizeof(logentry_t));

    entry->uid = getuid();
    entry->filename = strdup(path);
    entry->date = strdup( datestr(time.tm_year + 1900, time.tm_mon + 1, time.tm_mday) );
    entry->time = strdup( timestr(time.tm_hour, time.tm_min, time.tm_sec) );
    entry->type = type;
    entry->action_denied = act;
    if(md5string){
        entry->fingerprint = strdup(md5string);
    }else entry->fingerprint = NULL;

    newfile_entry(entry);
    return;
}

void newfile_entry(logentry_t *entry){

    FILE *fp;
    
    /* append data at the end of the file */
    fp = fopen("logfile.csv","a"); // not the original... ??
    if(fp == NULL){
        printf("logfile update failed\n");
        return;
    }

    fprintf(fp,"%d, %s, %s, %s, %d, %d, %s\n", entry->uid, entry->filename, 
    entry->date, entry->time, entry->type, entry->action_denied, entry->fingerprint);    

    fclose(fp);
}