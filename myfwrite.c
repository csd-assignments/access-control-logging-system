#define _GNU_SOURCE

#include "logfile.h"

/*
* content: pointer to the array of elements to be written
* size: the size in bytes of each element to be written
* nmemb: the number of elements, each one with a size of size bytes
* fd: pointer to a FILE object that specifies an output stream
*/
size_t fwrite(const void *content, size_t size, size_t nmemb, FILE *fd) {


    char path[1024], result[1024];
    int n;
    time_t t = time(NULL);
    struct tm time = *localtime(&t);

    unsigned char digest[16];
    char *md5string = (char*)malloc(33);
    MD5_CTX context;

    printf("In our own fwrite, writing %s\n", (char*)content);

    n = fileno(fd);

    /* Read out the link to our file descriptor. */
    sprintf(path, "/proc/self/fd/%d", n);
    memset(result, 0, sizeof(result));
    readlink(path, result, sizeof(result)-1);

    /* workig on fingerprint.. */
    MD5_Init(&context);
    MD5_Update(&context, content, sizeof(content));
    MD5_Final(digest, &context);

    for (n = 0; n < 16; ++n) {
        snprintf(&(md5string[n*2]), 16*2, "%02x", (unsigned int)digest[n]);
    }
    //printf("md5string: %s\n", md5string);

    logfile_update(result, time, 2, 0, md5string);

    size_t (*original_fwrite)(const void*, size_t, size_t, FILE*);
    original_fwrite = dlsym(RTLD_NEXT, "fwrite");
    
    return (*original_fwrite)(content, size, nmemb, fd);
}