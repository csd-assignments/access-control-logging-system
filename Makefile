CFLAGS = -g -Wall
LIBSSL = -lssl -lcrypto

all: main myfopen.so myfwrite.so monitor

main: main.c
	gcc $(CFLAGS) -o main main.c

myfopen.so: myfopen.c logfile.c logfile.h
	gcc $(CFLAGS) -fPIC -shared -o myfopen.so myfopen.c -ldl $(LIBSSL) logfile.c

myfwrite.so: myfwrite.c logfile.c logfile.h
	gcc $(CFLAGS) -fPIC -shared -o myfwrite.so myfwrite.c -ldl $(LIBSSL) logfile.c

monitor: monitor.c monitor.h
	gcc  $(CFLAGS) -o monitor monitor.c

clean:
	rm main
	rm myfopen.so
	rm myfwrite.so
	rm monitor

clt:
	rm test.txt test1.txt logfile.csv