#include <stdio.h>
#include <dlfcn.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

int main(){

    char *str = "test1 content";
    FILE *fd, *fd1, *fd2;

    //logfile_create();

    /* open gia write kai den yparxei */
    fd = fopen("test.txt", "w");
    fwrite("marvel", 1, strlen("marvel"), fd);
    fclose(fd);

    /* open gia read kai yparxei */
    fd = fopen("test.txt", "r");
    if(fd == NULL){
        printf("fopen() failed\n");
        return 1;
    }

    fclose(fd);

    /* open gia write kai den yparxei */
    fd1 = fopen("test1.txt", "w");
    fwrite("test1 content", 1, strlen("test1 content"), fd1);
    fclose(fd1);

    /* open gia read kai yparxei */
    fd1 = fopen("test1.txt", "r");
    fclose(fd1);

    /* open gia write kai yparxei */
    fd1 = fopen("test1.txt", "w");
    fwrite(str, 1, strlen(str), fd1);
    fclose(fd1);

    fd2 = fopen("test2.txt", "w");
    fwrite("test2 content", 1, strlen("test2 content"), fd2);
    fclose(fd2);

    /* open gia read kai den yparxei */
    fd = fopen("test3.txt", "r");
    if(fd == NULL){
        printf("fopen() failed\n");
        return 1;
    }

    fclose(fd);
 
    return 0;
}